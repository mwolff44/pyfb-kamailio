============
Installation
============

At the command line::

    $ easy_install pyfb-kamailio

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv pyfb-kamailio
    $ pip install pyfb-kamailio
