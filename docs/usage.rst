=====
Usage
=====

To use Pyfb-kamailio in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'pyfb_kamailio.apps.PyfbKamailioConfig',
        ...
    )

Add Pyfb-kamailio's URL patterns:

.. code-block:: python

    from pyfb_kamailio import urls as pyfb_kamailio_urls


    urlpatterns = [
        ...
        url(r'^', include(pyfb_kamailio_urls)),
        ...
    ]
