# -*- coding: utf-8
from django.apps import AppConfig


class PyfbKamailioConfig(AppConfig):
    name = 'pyfb_kamailio'
