Django>=2.2.9,<3

# Useful model mixins and utilities such as TimeStampedModel and Choices
# https://django-model-utils.readthedocs.org/en/latest/
# BSD
django-model-utils>=2.0

djangorestframework>=3.7.7
django-crispy-forms>=1.7.0
