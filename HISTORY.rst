.. :changelog:

History
-------

1.2.2 (2020-05-27)
++++++++++++++++++

* Update acc detail view

1.2.1 (2020-05-20)
++++++++++++++++++

* add cdr_id field in acc cdr

1.2.0 (2020-05-17)
++++++++++++++++++

* add fields in acc cdr
* update admin views

1.1.0 (2020-05-14)
++++++++++++++++++

* add fields in acc

1.0.3 (2020-03-21)
++++++++++++++++++

* update test
* update uacreg table to be compliant with 5.3 kamailio version

1.0.2 (2019-07-10)
++++++++++++++++++

* admin tweaks for uacreg module

1.0.1 (2019-06-21)
++++++++++++++++++

* Correct fk representation of domain
* Admin enhancements

1.0.0 (2019-05-23)
++++++++++++++++++

* add domain table
* add translations

0.9.4 (2019-03-06)
++++++++++++++++++

* add fields in missed_calls table

0.9.3 (2019-02-26)
++++++++++++++++++

* add topo tables

0.9.2 (2019-02-25)
++++++++++++++++++

* correct field name in ACC table
* add more fielads in ACC and CDR tables
* admin tweaks

0.9.1 (2019-01-30)
++++++++++++++++++

* upload fixtures from migration for initial setup
* new table for statistics

0.9.0 (2018-11-15)
++++++++++++++++++

* First release on PyPI.
