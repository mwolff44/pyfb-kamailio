#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase
from django.urls import reverse
from django.test import Client

from pyfb_kamailio.models import Dialog, DialogVar, Acc, AccCdr, MissedCall, UacReg, Trusted, Version, Location, LocationAttrs, UserBlackList, GlobalBlackList, SpeedDial, PipeLimit, Mtree, Mtrees, Htable, RtpEngine


def create_dialog(**kwargs):
    defaults = {}
    defaults["hash_entry"] = 1
    defaults["hash_id"] = 1
    defaults["callid"] = "callid"
    defaults["from_uri"] = "from_uri"
    defaults["from_tag"] = "from_tag"
    defaults["to_uri"] = "to_uri"
    defaults["to_tag"] = "to_tag"
    defaults["caller_cseq"] = "caller_cseq"
    defaults["callee_cseq"] = "callee_cseq"
    defaults["caller_route_set"] = "caller_route_set"
    defaults["callee_route_set"] = "callee_route_set"
    defaults["caller_contact"] = "caller_contact"
    defaults["callee_contact"] = "callee_contact"
    defaults["caller_sock"] = "caller_sock"
    defaults["callee_sock"] = "callee_sock"
    defaults["state"] = 1
    defaults["start_time"] = 1
    defaults["timeout"] = 1
    defaults["sflags"] = 1
    defaults["iflags"] = 1
    defaults["toroute_name"] = "toroute_name"
    defaults["req_uri"] = "req_uri"
    defaults["xdata"] = "xdata"
    defaults.update(**kwargs)
    return Dialog.objects.create(**defaults)


def create_dialogvar(**kwargs):
    defaults = {}
    defaults["hash_entry"] = 1
    defaults["hash_id"] = 1
    defaults["dialog_key"] = "dialog_key"
    defaults["dialog_value"] = "dialog_value"
    defaults.update(**kwargs)
    return DialogVar.objects.create(**defaults)


def create_acc(**kwargs):
    defaults = {}
    defaults["method"] = "method"
    defaults["from_tag"] = "from_tag"
    defaults["to_tag"] = "to_tag"
    defaults["callid"] = "callid"
    defaults["sip_code"] = "404"
    defaults["sip_reason"] = "sip_reason"
    defaults["time"] = '2013-11-11'
    defaults["time_attr"] = 1
    defaults["time_exten"] = 1
    defaults.update(**kwargs)
    return Acc.objects.create(**defaults)


def create_acccdr(**kwargs):
    defaults = {}
    defaults["start_time"] = '2013-11-11'
    defaults["end_time"] = '2013-11-11'
    defaults["duration"] = '1.5'
    defaults.update(**kwargs)
    return AccCdr.objects.create(**defaults)


def create_missedcall(**kwargs):
    defaults = {}
    defaults["method"] = "method"
    defaults["from_tag"] = "from_tag"
    defaults["to_tag"] = "to_tag"
    defaults["callid"] = "callid"
    defaults["sip_code"] = "404"
    defaults["sip_reason"] = "sip_reason"
    defaults["time"] = '2013-11-11'
    defaults.update(**kwargs)
    return MissedCall.objects.create(**defaults)


def create_uacreg(**kwargs):
    defaults = {}
    defaults["l_uuid"] = "l_uuid"
    defaults["l_username"] = "l_username"
    defaults["l_domain"] = "l_domain"
    defaults["r_username"] = "r_username"
    defaults["r_domain"] = "r_domain"
    defaults["realm"] = "realm"
    defaults["auth_username"] = "auth_username"
    defaults["auth_password"] = "auth_password"
    defaults["auth_ha1"] = "auth_ha1"
    defaults["auth_proxy"] = "auth_proxy"
    defaults["expires"] = 1
    defaults["flags"] = 1
    defaults["reg_delay"] = 1
    defaults["socket"]= "192.0.2.1"
    defaults.update(**kwargs)
    return UacReg.objects.create(**defaults)


def create_trusted(**kwargs):
    defaults = {}
    defaults["src_ip"] = "any"
    defaults["proto"] = "any"
    defaults["from_pattern"] = "from_pattern"
    defaults["ruri_pattern"] = "ruri_pattern"
    defaults["tag"] = "tag"
    defaults["priority"] = 1
    defaults.update(**kwargs)
    return Trusted.objects.create(**defaults)


def create_version(**kwargs):
    defaults = {}
    defaults["table_name"] = "table_name"
    defaults["table_version"] = 1
    defaults.update(**kwargs)
    return Version.objects.create(**defaults)


def create_location(**kwargs):
    defaults = {}
    defaults["ruid"] = "ruid"
    defaults["username"] = "username"
    defaults["domain"] = "domain"
    defaults["contact"] = "contact"
    defaults["received"] = "received"
    defaults["path"] = "path"
    defaults["expires"] = '2013-11-11'
    defaults["q"] = 10.10
    defaults["callid"] = "callid"
    defaults["cseq"] = 1
    defaults["last_modified"] = '2013-11-11'
    defaults["flags"] = 1
    defaults["cflags"] = 1
    defaults["user_agent"] = "user_agent"
    defaults["socket"] = "socket"
    defaults["methods"] = 1
    defaults["instance"] = "instance"
    defaults["reg_id"] = 1
    defaults["server_id"] = 1
    defaults["connection_id"] = 1
    defaults["keepalive"] = 1
    defaults["partition"] = 1
    defaults.update(**kwargs)
    return Location.objects.create(**defaults)


def create_locationattrs(**kwargs):
    defaults = {}
    defaults["ruid"] = "ruid"
    defaults["username"] = "username"
    defaults["domain"] = "domain"
    defaults["aname"] = "aname"
    defaults["atype"] = 1
    defaults["avalue"] = "avalue"
    defaults["last_modified"] = '2013-11-11'
    defaults.update(**kwargs)
    return LocationAttrs.objects.create(**defaults)


def create_userblacklist(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["domain"] = "domain"
    defaults["prefix"] = "prefix"
    defaults["whitelist"] = "0"
    defaults.update(**kwargs)
    return UserBlackList.objects.create(**defaults)


def create_globalblacklist(**kwargs):
    defaults = {}
    defaults["prefix"] = "prefix"
    defaults["whitelist"] = "0"
    defaults["description"] = "description"
    defaults.update(**kwargs)
    return GlobalBlackList.objects.create(**defaults)


def create_speeddial(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["domain"] = "domain"
    defaults["sd_username"] = "sd_username"
    defaults["sd_domain"] = "sd_domain"
    defaults["new_uri"] = "new_uri"
    defaults["fname"] = "fname"
    defaults["lname"] = "lname"
    defaults["description"] = "description"
    defaults.update(**kwargs)
    return SpeedDial.objects.create(**defaults)


def create_pipelimit(**kwargs):
    defaults = {}
    defaults["pipeid"] = "pipeid"
    defaults["algorithm"] = "NOP"
    defaults["plimit"] = 1
    defaults.update(**kwargs)
    return PipeLimit.objects.create(**defaults)


def create_mtree(**kwargs):
    defaults = {}
    defaults["tprefix"] = "tprefix"
    defaults["tvalue"] = "tvalue"
    defaults.update(**kwargs)
    return Mtree.objects.create(**defaults)


def create_mtrees(**kwargs):
    defaults = {}
    defaults["tname"] = "tname"
    defaults["tprefix"] = "tprefix"
    defaults["tvalue"] = "tvalue"
    defaults.update(**kwargs)
    return Mtrees.objects.create(**defaults)


def create_htable(**kwargs):
    defaults = {}
    defaults["key_name"] = "key_name"
    defaults["key_type"] = 1
    defaults["value_type"] = 1
    defaults["key_value"] = "key_value"
    defaults["expires"] = 1
    defaults.update(**kwargs)
    return Htable.objects.create(**defaults)


def create_rtpengine(**kwargs):
    defaults = {}
    defaults["setid"] = 1
    defaults["url"] = "url"
    defaults["weight"] = 1
    defaults["disabled"] = 1
    defaults["stamp"] = '2013-11-11'
    defaults.update(**kwargs)
    return RtpEngine.objects.create(**defaults)


class DialogViewTest(TestCase):
    '''
    Tests for Dialog
    '''
    def setUp(self):
        self.client = Client()

    def test_list_dialog(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_dialog_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_dialog(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_dialog_create')
        data = {
            "hash_entry": 1,
            "hash_id": 1,
            "callid": "callid",
            "from_uri": "from_uri",
            "from_tag": "from_tag",
            "to_uri": "to_uri",
            "to_tag": "to_tag",
            "caller_cseq": "caller_cseq",
            "callee_cseq": "callee_cseq",
            "caller_route_set": "caller_route_set",
            "callee_route_set": "callee_route_set",
            "caller_contact": "caller_contact",
            "callee_contact": "callee_contact",
            "caller_sock": "caller_sock",
            "callee_sock": "callee_sock",
            "state": 1,
            "start_time": 1,
            "timeout": 1,
            "sflags": 1,
            "iflags": 1,
            "toroute_name": "toroute_name",
            "req_uri": "req_uri",
            "xdata": "xdata",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_dialog(self):
        dialog = create_dialog()
        url = reverse('pyfb-kamailio:pyfb_kamailio_dialog_detail', args=[dialog.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_dialog(self):
        dialog = create_dialog()
        data = {
            "hash_entry": 1,
            "hash_id": 1,
            "callid": "callid",
            "from_uri": "from_uri",
            "from_tag": "from_tag",
            "to_uri": "to_uri",
            "to_tag": "to_tag",
            "caller_cseq": "caller_cseq",
            "callee_cseq": "callee_cseq",
            "caller_route_set": "caller_route_set",
            "callee_route_set": "callee_route_set",
            "caller_contact": "caller_contact",
            "callee_contact": "callee_contact",
            "caller_sock": "caller_sock",
            "callee_sock": "callee_sock",
            "state": 1,
            "start_time": 1,
            "timeout": 1,
            "sflags": 1,
            "iflags": 1,
            "toroute_name": "toroute_name",
            "req_uri": "req_uri",
            "xdata": "xdata",
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_dialog_update', args=[dialog.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DialogVarViewTest(TestCase):
    '''
    Tests for DialogVar
    '''
    def setUp(self):
        self.client = Client()

    def test_list_dialogvar(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_dialogvar_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_dialogvar(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_dialogvar_create')
        data = {
            "hash_entry": 1,
            "hash_id": 1,
            "dialog_key": "dialog_key",
            "dialog_value": "dialog_value",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_dialogvar(self):
        dialogvar = create_dialogvar()
        url = reverse('pyfb-kamailio:pyfb_kamailio_dialogvar_detail', args=[dialogvar.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_dialogvar(self):
        dialogvar = create_dialogvar()
        data = {
            "hash_entry": 1,
            "hash_id": 1,
            "dialog_key": "dialog_key",
            "dialog_value": "dialog_value",
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_dialogvar_update', args=[dialogvar.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class AccViewTest(TestCase):
    '''
    Tests for Acc
    '''
    def setUp(self):
        self.client = Client()

    def test_list_acc(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_acc_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_acc(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_acc_create')
        data = {
            "method": "method",
            "from_tag": "from_tag",
            "to_tag": "to_tag",
            "callid": "callid",
            "sip_code": "404",
            "sip_reason": "sip_reason",
            "time": '2013-11-11',
            "time_attr": 1,
            "time_exten": 1,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_acc(self):
        acc = create_acc()
        url = reverse('pyfb-kamailio:pyfb_kamailio_acc_detail', args=[acc.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_acc(self):
        acc = create_acc()
        data = {
            "method": "method",
            "from_tag": "from_tag",
            "to_tag": "to_tag",
            "callid": "callid",
            "sip_code": "404",
            "sip_reason": "sip_reason",
            "time": '2013-11-11',
            "time_attr": 1,
            "time_exten": 1,
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_acc_update', args=[acc.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class AccCdrViewTest(TestCase):
    '''
    Tests for AccCdr
    '''
    def setUp(self):
        self.client = Client()

    def test_list_acccdr(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_acccdr_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_acccdr(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_acccdr_create')
        data = {
            "start_time": '2013-11-11',
            "end_time": '2013-11-11',
            "duration": '1.5',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_acccdr(self):
        acccdr = create_acccdr()
        url = reverse('pyfb-kamailio:pyfb_kamailio_acccdr_detail', args=[acccdr.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_acccdr(self):
        acccdr = create_acccdr()
        data = {
            "start_time": '2013-11-11',
            "end_time": '2013-11-11',
            "duration": '1.5',
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_acccdr_update', args=[acccdr.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class MissedCallViewTest(TestCase):
    '''
    Tests for MissedCall
    '''
    def setUp(self):
        self.client = Client()

    def test_list_missedcall(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_missedcall_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_missedcall(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_missedcall_create')
        data = {
            "method": "method",
            "from_tag": "from_tag",
            "to_tag": "to_tag",
            "callid": "callid",
            "sip_code": "404",
            "sip_reason": "sip_reason",
            "time": '2013-11-11',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_missedcall(self):
        missedcall = create_missedcall()
        url = reverse('pyfb-kamailio:pyfb_kamailio_missedcall_detail', args=[missedcall.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_missedcall(self):
        missedcall = create_missedcall()
        data = {
            "method": "method",
            "from_tag": "from_tag",
            "to_tag": "to_tag",
            "callid": "callid",
            "sip_code": "404",
            "sip_reason": "sip_reason",
            "time": '2013-11-11',
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_missedcall_update', args=[missedcall.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class UacRegViewTest(TestCase):
    '''
    Tests for UacReg
    '''
    def setUp(self):
        self.client = Client()

    def test_list_uacreg(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_uacreg_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_uacreg(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_uacreg_create')
        data = {
            "l_uuid": "l_uuid",
            "l_username": "l_username",
            "l_domain": "l_domain",
            "r_username": "r_username",
            "r_domain": "r_domain",
            "realm": "realm",
            "auth_username": "auth_username",
            "auth_password": "auth_password",
            "auth_ha1": "auth_ha1",
            "auth_proxy": "auth_proxy",
            "expires": 1,
            "flags": 1,
            "reg_delay": 1,
            "socket": "192.0.2.1",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_uacreg(self):
        uacreg = create_uacreg()
        url = reverse('pyfb-kamailio:pyfb_kamailio_uacreg_detail', args=[uacreg.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_uacreg(self):
        uacreg = create_uacreg()
        data = {
            "l_uuid": "l_uuid",
            "l_username": "l_username",
            "l_domain": "l_domain",
            "r_username": "r_username",
            "r_domain": "r_domain",
            "realm": "realm",
            "auth_username": "auth_username",
            "auth_password": "auth_password",
            "auth_ha1": "auth_ha1",
            "auth_proxy": "auth_proxy",
            "expires": 1,
            "flags": 1,
            "reg_delay": 1,
            "socket": "192.0.2.1",
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_uacreg_update', args=[uacreg.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class TrustedViewTest(TestCase):
    '''
    Tests for Trusted
    '''
    def setUp(self):
        self.client = Client()

    def test_list_trusted(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_trusted_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_trusted(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_trusted_create')
        data = {
            "src_ip": "any",
            "proto": "any",
            "from_pattern": "from_pattern",
            "ruri_pattern": "ruri_pattern",
            "tag": "tag",
            "priority": 1,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_trusted(self):
        trusted = create_trusted()
        url = reverse('pyfb-kamailio:pyfb_kamailio_trusted_detail', args=[trusted.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_trusted(self):
        trusted = create_trusted()
        data = {
            "src_ip": "any",
            "proto": "any",
            "from_pattern": "from_pattern",
            "ruri_pattern": "ruri_pattern",
            "tag": "tag",
            "priority": 1,
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_trusted_update', args=[trusted.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class VersionViewTest(TestCase):
    '''
    Tests for Version
    '''
    def setUp(self):
        self.client = Client()

    def test_list_version(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_version_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_version(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_version_create')
        data = {
            "table_name": "table_name",
            "table_version": 1,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_version(self):
        version = create_version()
        url = reverse('pyfb-kamailio:pyfb_kamailio_version_detail', args=[version.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_version(self):
        version = create_version()
        data = {
            "table_name": "table_name",
            "table_version": 1,
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_version_update', args=[version.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LocationViewTest(TestCase):
    '''
    Tests for Location
    '''
    def setUp(self):
        self.client = Client()

    def test_list_location(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_location_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_location(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_location_create')
        data = {
            "ruid": "ruid",
            "username": "username",
            "domain": "domain",
            "contact": "contact",
            "received": "received",
            "path": "path",
            "expires": '2013-11-11',
            "q": 10.10,
            "callid": "callid",
            "cseq": 1,
            "last_modified": '2013-11-11',
            "flags": 1,
            "cflags": 1,
            "user_agent": "user_agent",
            "socket": "socket",
            "methods": 1,
            "instance": "instance",
            "reg_id": 1,
            "server_id": 1,
            "connection_id": 1,
            "keepalive": 1,
            "partition": 1,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_location(self):
        location = create_location()
        url = reverse('pyfb-kamailio:pyfb_kamailio_location_detail', args=[location.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_location(self):
        location = create_location()
        data = {
            "ruid": "ruid",
            "username": "username",
            "domain": "domain",
            "contact": "contact",
            "received": "received",
            "path": "path",
            "expires": '2013-11-11',
            "q": 10.10,
            "callid": "callid",
            "cseq": 1,
            "last_modified": '2013-11-11',
            "flags": 1,
            "cflags": 1,
            "user_agent": "user_agent",
            "socket": "socket",
            "methods": 1,
            "instance": "instance",
            "reg_id": 1,
            "server_id": 1,
            "connection_id": 1,
            "keepalive": 1,
            "partition": 1,
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_location_update', args=[location.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class LocationAttrsViewTest(TestCase):
    '''
    Tests for LocationAttrs
    '''
    def setUp(self):
        self.client = Client()

    def test_list_locationattrs(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_locationattrs_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_locationattrs(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_locationattrs_create')
        data = {
            "ruid": "ruid",
            "username": "username",
            "domain": "domain",
            "aname": "aname",
            "atype": 1,
            "avalue": "avalue",
            "last_modified": '2013-11-11',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_locationattrs(self):
        locationattrs = create_locationattrs()
        url = reverse('pyfb-kamailio:pyfb_kamailio_locationattrs_detail', args=[locationattrs.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_locationattrs(self):
        locationattrs = create_locationattrs()
        data = {
            "ruid": "ruid",
            "username": "username",
            "domain": "domain",
            "aname": "aname",
            "atype": 1,
            "avalue": "avalue",
            "last_modified": '2013-11-11',
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_locationattrs_update', args=[locationattrs.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class UserBlackListViewTest(TestCase):
    '''
    Tests for UserBlackList
    '''
    def setUp(self):
        self.client = Client()

    def test_list_userblacklist(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_userblacklist_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_userblacklist(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_userblacklist_create')
        data = {
            "username": "username",
            "domain": "domain",
            "prefix": "prefix",
            "whitelist": 0,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_userblacklist(self):
        userblacklist = create_userblacklist()
        url = reverse('pyfb-kamailio:pyfb_kamailio_userblacklist_detail', args=[userblacklist.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_userblacklist(self):
        userblacklist = create_userblacklist()
        data = {
            "username": "username",
            "domain": "domain",
            "prefix": "prefix",
            "whitelist": 0,
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_userblacklist_update', args=[userblacklist.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class GlobalBlackListViewTest(TestCase):
    '''
    Tests for GlobalBlackList
    '''
    def setUp(self):
        self.client = Client()

    def test_list_globalblacklist(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_globalblacklist_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_globalblacklist(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_globalblacklist_create')
        data = {
            "prefix": "prefix",
            "whitelist": 0,
            "description": "description",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_globalblacklist(self):
        globalblacklist = create_globalblacklist()
        url = reverse('pyfb-kamailio:pyfb_kamailio_globalblacklist_detail', args=[globalblacklist.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_globalblacklist(self):
        globalblacklist = create_globalblacklist()
        data = {
            "prefix": "prefix",
            "whitelist": 0,
            "description": "description",
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_globalblacklist_update', args=[globalblacklist.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class SpeedDialViewTest(TestCase):
    '''
    Tests for SpeedDial
    '''
    def setUp(self):
        self.client = Client()

    def test_list_speeddial(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_speeddial_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_speeddial(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_speeddial_create')
        data = {
            "username": "username",
            "domain": "domain",
            "sd_username": "sd_username",
            "sd_domain": "sd_domain",
            "new_uri": "new_uri",
            "fname": "fname",
            "lname": "lname",
            "description": "description",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_speeddial(self):
        speeddial = create_speeddial()
        url = reverse('pyfb-kamailio:pyfb_kamailio_speeddial_detail', args=[speeddial.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_speeddial(self):
        speeddial = create_speeddial()
        data = {
            "username": "username",
            "domain": "domain",
            "sd_username": "sd_username",
            "sd_domain": "sd_domain",
            "new_uri": "new_uri",
            "fname": "fname",
            "lname": "lname",
            "description": "description",
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_speeddial_update', args=[speeddial.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class PipeLimitViewTest(TestCase):
    '''
    Tests for PipeLimit
    '''
    def setUp(self):
        self.client = Client()

    def test_list_pipelimit(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_pipelimit_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_pipelimit(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_pipelimit_create')
        data = {
            "pipeid": "pipeid",
            "algorithm": "NOP",
            "plimit": 1,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_pipelimit(self):
        pipelimit = create_pipelimit()
        url = reverse('pyfb-kamailio:pyfb_kamailio_pipelimit_detail', args=[pipelimit.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_pipelimit(self):
        pipelimit = create_pipelimit()
        data = {
            "pipeid": "pipeid",
            "algorithm": "NOP",
            "plimit": 1,
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_pipelimit_update', args=[pipelimit.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class MtreeViewTest(TestCase):
    '''
    Tests for Mtree
    '''
    def setUp(self):
        self.client = Client()

    def test_list_mtree(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_mtree_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_mtree(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_mtree_create')
        data = {
            "tprefix": "tprefix",
            "tvalue": "tvalue",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_mtree(self):
        mtree = create_mtree()
        url = reverse('pyfb-kamailio:pyfb_kamailio_mtree_detail', args=[mtree.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_mtree(self):
        mtree = create_mtree()
        data = {
            "tprefix": "tprefix",
            "tvalue": "tvalue",
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_mtree_update', args=[mtree.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class MtreesViewTest(TestCase):
    '''
    Tests for Mtrees
    '''
    def setUp(self):
        self.client = Client()

    def test_list_mtrees(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_mtrees_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_mtrees(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_mtrees_create')
        data = {
            "tname": "tname",
            "tprefix": "tprefix",
            "tvalue": "tvalue",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_mtrees(self):
        mtrees = create_mtrees()
        url = reverse('pyfb-kamailio:pyfb_kamailio_mtrees_detail', args=[mtrees.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_mtrees(self):
        mtrees = create_mtrees()
        data = {
            "tname": "tname",
            "tprefix": "tprefix",
            "tvalue": "tvalue",
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_mtrees_update', args=[mtrees.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class HtableViewTest(TestCase):
    '''
    Tests for Htable
    '''
    def setUp(self):
        self.client = Client()

    def test_list_htable(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_htable_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_htable(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_htable_create')
        data = {
            "key_name": "key_name",
            "key_type": 1,
            "value_type": 1,
            "key_value": "key_value",
            "expires": 1,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_htable(self):
        htable = create_htable()
        url = reverse('pyfb-kamailio:pyfb_kamailio_htable_detail', args=[htable.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_htable(self):
        htable = create_htable()
        data = {
            "key_name": "key_name",
            "key_type": 1,
            "value_type": 1,
            "key_value": "key_value",
            "expires": 1,
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_htable_update', args=[htable.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class RtpEngineViewTest(TestCase):
    '''
    Tests for RtpEngine
    '''
    def setUp(self):
        self.client = Client()

    def test_list_rtpengine(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_rtpengine_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_rtpengine(self):
        url = reverse('pyfb-kamailio:pyfb_kamailio_rtpengine_create')
        data = {
            "setid": 1,
            "url": "url",
            "weight": 1,
            "disabled": 1,
            "stamp": '2013-11-11',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_rtpengine(self):
        rtpengine = create_rtpengine()
        url = reverse('pyfb-kamailio:pyfb_kamailio_rtpengine_detail', args=[rtpengine.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_rtpengine(self):
        rtpengine = create_rtpengine()
        data = {
            "setid": 1,
            "url": "url",
            "weight": 1,
            "disabled": 1,
            "stamp": '2013-11-11',
        }
        url = reverse('pyfb-kamailio:pyfb_kamailio_rtpengine_update', args=[rtpengine.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
